package barreto.alessandro.githubjavapop.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import barreto.alessandro.githubjavapop.R
import barreto.alessandro.githubjavapop.adapter.EndlessRecyclerViewScrollListener
import barreto.alessandro.githubjavapop.adapter.RepositoryAdapter
import barreto.alessandro.githubjavapop.model.Repositories
import barreto.alessandro.githubjavapop.model.Repository
import barreto.alessandro.githubjavapop.rest_http.RestClient
import com.google.gson.Gson
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), RestClient.ResponseServe {

    private val STATE_LIST = "list"
    private val STATE_PAGE = "page"
    private val STATE_REQUEST = "request"
    private var page = 1
    private var listRepositories = mutableListOf<Repository>()
    private var adapterList : RepositoryAdapter? = null
    private var isRequest = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState != null){
            page = savedInstanceState.getInt(STATE_PAGE)
            listRepositories = savedInstanceState.getParcelableArrayList(STATE_LIST)
            isRequest = savedInstanceState.getBoolean(STATE_REQUEST)
            if (isRequest)RestClient.getRepositories(page,this,this)
        }else{
            RestClient.getRepositories(page,this,this)
        }
        initView()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBoolean(STATE_REQUEST,isRequest)
        outState?.putInt(STATE_PAGE,page)
        outState?.putParcelableArrayList(STATE_LIST, ArrayList(listRepositories))
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isRequest)
            RestClient.cancelRequest()
    }

    private fun initView(){
        val layoutManager = LinearLayoutManager(this@MainActivity)
        rvListMain.layoutManager = layoutManager
        adapterList = RepositoryAdapter(listRepositories,{ repository -> Unit
            startActivity(PullRequestActivity.newIntent(this@MainActivity,repository))
        })
        rvListMain.adapter = adapterList
        rvListMain.addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager){
            override fun onLoadMore(pag: Int, totalItemsCount: Int, view: RecyclerView?) {
                RestClient.getRepositories(page,this@MainActivity,this@MainActivity)
            }
        })
        buttonConnection.setOnClickListener({ RestClient.getRepositories(page,this@MainActivity,this@MainActivity) })
    }

    private fun showView(flag : Boolean) {
        if (flag){
            llNoConnection.visibility = View.GONE
            flListMain.visibility = View.VISIBLE
        }else{
            llNoConnection.visibility = View.VISIBLE
            flListMain.visibility = View.GONE
        }
    }

    override fun onStartRequest() {
        showView(true)
        isRequest = true
        progressBar.visibility = View.VISIBLE
    }

    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: String?) {
        isRequest = false
        val data: Repositories = Gson().fromJson(response, Repositories::class.java)
        listRepositories.addAll(data.repositories)
        adapterList?.notifyDataSetChanged()
        page++
        progressBar.visibility = View.GONE
    }

    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
        isRequest = false
        progressBar.visibility = View.GONE
        Snackbar.make(containerMain,R.string.erro_request, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_snack,{
            RestClient.getRepositories(page,this@MainActivity,this@MainActivity) }
        ).show()
    }

    override fun noConnection() {
        showView(false)
    }
}
