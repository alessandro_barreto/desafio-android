package barreto.alessandro.githubjavapop.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import barreto.alessandro.githubjavapop.R
import barreto.alessandro.githubjavapop.adapter.PullRequestAdapter
import barreto.alessandro.githubjavapop.model.PullRequest
import barreto.alessandro.githubjavapop.model.Repository
import barreto.alessandro.githubjavapop.rest_http.RestClient
import com.google.gson.Gson
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_pull_request.*

class PullRequestActivity : AppCompatActivity(), RestClient.ResponseServe {

    private val STATE_LIST = "list"
    private var listPullRequests = mutableListOf<PullRequest>()
    private var adapter : PullRequestAdapter? = null
    private var owner : String = ""
    private var repositoryName : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_request)

        owner = intent.getStringExtra(INTENT_OWNER) ?: throw IllegalStateException("field $INTENT_OWNER missing in Intent")
        repositoryName = intent.getStringExtra(INTENT_REPOSITORY) ?: throw IllegalStateException("field $INTENT_REPOSITORY missing in Intent")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = repositoryName

        if (savedInstanceState != null){
            listPullRequests = savedInstanceState.getParcelableArrayList(STATE_LIST)
        }
        else{
            RestClient.getPullRequests(owner, repositoryName,this,this)
        }
        initView()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(STATE_LIST, ArrayList( listPullRequests ))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun initView(){
        rvListPull.layoutManager = LinearLayoutManager(this@PullRequestActivity)
        adapter = PullRequestAdapter(listPullRequests,{pullRequest -> Unit
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(pullRequest.htmlUrl)
            startActivity(i)
        })
        rvListPull.adapter = adapter
    }

    private fun showView(flag : Boolean){
        if (flag){
            llContainerListEmpty.visibility = View.GONE
            frContainerMain.visibility = View.VISIBLE
        }else{
            llContainerListEmpty.visibility = View.VISIBLE
            frContainerMain.visibility = View.GONE
        }
    }

    override fun onStartRequest() {
        showView(true)
        progressBar.visibility = View.VISIBLE
    }

    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: String?) {
        val data : List<PullRequest> = Gson().fromJson(response.toString(),Array<PullRequest>::class.java).toList()
        listPullRequests.addAll(data)
        adapter?.notifyDataSetChanged()
        progressBar.visibility = View.GONE
        if (listPullRequests.isEmpty()) showView(false)
    }

    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
        showView(false)
        progressBar.visibility = View.GONE
        Snackbar.make(containerMain,R.string.erro_request, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_snack,{
            RestClient.getPullRequests(owner, repositoryName,this@PullRequestActivity,this@PullRequestActivity)
        }).show()
    }

    override fun noConnection() {
        showView(false)
        Snackbar.make(containerMain,R.string.no_connection,Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_snack,{
            RestClient.getPullRequests(owner, repositoryName,this@PullRequestActivity,this@PullRequestActivity)
        }).show()
    }

    companion object {
        private val INTENT_OWNER = "owner"
        private val INTENT_REPOSITORY = "repository"
        fun newIntent(context: Context, repository: Repository): Intent {
            val intent = Intent(context, PullRequestActivity::class.java)
            intent.putExtra(INTENT_OWNER, repository.owner.login)
            intent.putExtra(INTENT_REPOSITORY,repository.name)
            return intent
        }
    }
}
