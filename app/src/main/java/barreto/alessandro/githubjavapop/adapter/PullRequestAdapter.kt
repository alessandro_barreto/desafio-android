package barreto.alessandro.githubjavapop.adapter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import barreto.alessandro.githubjavapop.R
import barreto.alessandro.githubjavapop.model.PullRequest
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_pull_request.view.*

/**
 * Created by AleBarreto on 12/26/17.
 */
class PullRequestAdapter(
        val listPullRequests: List<PullRequest>, val onClick : (PullRequest) -> Unit)
    : RecyclerView.Adapter<PullRequestAdapter.PullRequestViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PullRequestViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_pull_request, parent, false)
        return PullRequestAdapter.PullRequestViewHolder(view)
    }

    override fun getItemCount(): Int { return this.listPullRequests.size }

    override fun onBindViewHolder(holder: PullRequestViewHolder?, position: Int) {

        val pullRequest = listPullRequests[position]
        val view = holder?.itemView

        with(view){
            this?.tv_title?.text = pullRequest.title
            if (pullRequest.body.isNullOrBlank()){
                this?.tv_desc?.text = view?.context?.getText(R.string.description)
            }else{
                this?.tv_desc?.text = pullRequest.body
            }
            this?.tv_username?.text = pullRequest.owner.login
            Picasso.with( this?.iv_user?.context ).load( pullRequest.owner.avatarUrl ).placeholder(R.drawable.no_image).resize(40,40).centerCrop().into(this?.iv_user)
            this?.setOnClickListener { onClick(pullRequest) }
        }
    }
    class PullRequestViewHolder(view: View): RecyclerView.ViewHolder(view)
}