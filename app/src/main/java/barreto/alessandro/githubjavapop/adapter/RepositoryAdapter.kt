package barreto.alessandro.githubjavapop.adapter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import barreto.alessandro.githubjavapop.R
import barreto.alessandro.githubjavapop.model.Repository
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_main.view.*

/**
 * Created by AleBarreto on 12/26/17.
 */
class RepositoryAdapter(
        val repositories : List<Repository>,
        val onClick : (Repository) -> Unit) : RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoryViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_main, parent, false)
        return RepositoryViewHolder(view)
    }

    override fun getItemCount(): Int { return this.repositories.size }

    override fun onBindViewHolder(holder: RepositoryViewHolder?, position: Int) {
        val repository = repositories[position]
        val view = holder?.itemView

        with(view){
            this?.tv_repo?.text = repository.name
            this?.tv_desc_repo?.text = repository.description
            this?.tv_stars?.text = repository.stargazersCount.toString()
            this?.tv_forks?.text = repository.forksCount.toString()
            this?.tv_username?.text = repository.owner.login
            Picasso.with( this?.iv_user?.context ).load( repository.owner.avatarUrl ).placeholder(R.drawable.no_image).resize(50,50).centerCrop().into(this?.iv_user)
            this?.setOnClickListener { onClick(repository) }
        }

    }
    class RepositoryViewHolder(view:View): RecyclerView.ViewHolder(view)

}