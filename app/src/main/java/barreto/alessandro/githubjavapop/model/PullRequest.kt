package barreto.alessandro.githubjavapop.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by AleBarreto on 12/26/17.
 */
data class PullRequest(
        val id: Int,
        @SerializedName("html_url") val htmlUrl: String,
        val title: String,
        @SerializedName("user") val owner: Owner,
        val body: String
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readParcelable<Owner>(Owner::class.java.classLoader),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeString(htmlUrl)
        writeString(title)
        writeParcelable(owner, 0)
        writeString(body)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<PullRequest> = object : Parcelable.Creator<PullRequest> {
            override fun createFromParcel(source: Parcel): PullRequest = PullRequest(source)
            override fun newArray(size: Int): Array<PullRequest?> = arrayOfNulls(size)
        }
    }
}