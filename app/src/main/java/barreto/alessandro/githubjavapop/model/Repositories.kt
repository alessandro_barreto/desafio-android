package barreto.alessandro.githubjavapop.model

import com.google.gson.annotations.SerializedName

/**
 * Created by AleBarreto on 12/26/17.
 */
data class Repositories(
        @SerializedName("items") val repositories : MutableList<Repository>
)