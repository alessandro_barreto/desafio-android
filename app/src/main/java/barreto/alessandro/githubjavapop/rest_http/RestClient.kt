package barreto.alessandro.githubjavapop.rest_http

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.JsonHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by AleBarreto on 12/26/17.
 * https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1
 * https://api.github.com/repos/<criador>/<repositório>/pulls
 */
class RestClient{

    interface ResponseServe{
        fun onStartRequest()
        fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: String?)
        fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?)
        fun noConnection()
    }

    companion object {
        private val BASE_URL : String = "https://api.github.com/"
        private val client: AsyncHttpClient = AsyncHttpClient()

        private lateinit var responseServer : ResponseServe

        private fun get (url : String, responseHandler: AsyncHttpResponseHandler){
            client.setUserAgent("Test API @AleBarreto")
            client.get(getAbsoluteUrl(url),null,responseHandler)
        }

        private fun getAbsoluteUrl( relativeUrl: String ) : String {
            return BASE_URL +relativeUrl
        }

        fun getRepositories(page: Int,res : ResponseServe,context: Context){
            responseServer = res
            if (isNetworkAvailable(context)){
                get("search/repositories?q=language:Java&sort=stars&page=$page", responseJsonHttp)
            }else{
                responseServer.noConnection()
            }
        }

        fun getPullRequests(owner:String, repositoryName: String,res : ResponseServe,context: Context ){
            responseServer = res
            if (isNetworkAvailable(context)){
                get("repos/$owner/$repositoryName/pulls", responseJsonHttp)
            }else{
                responseServer.noConnection()
            }
        }

        private val responseJsonHttp = object : JsonHttpResponseHandler(){
            override fun onStart() {
                responseServer.onStartRequest()
            }
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                responseServer.onSuccess(statusCode,headers,response.toString())
            }
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONArray?) {
                responseServer.onSuccess(statusCode,headers,response.toString())
            }
            override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                responseServer.onFailure(statusCode,headers,errorResponse.toString(),throwable)
            }
        }

        fun cancelRequest(){
            client.cancelAllRequests(true)
        }

        private fun isNetworkAvailable(context: Context): Boolean {
            val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networks = connectivity.allNetworks
            return networks
                    .map { connectivity.getNetworkInfo(it) }
                    .any { it.state == NetworkInfo.State.CONNECTED }
        }

    }

}